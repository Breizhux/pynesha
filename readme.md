# PyNeSha

Python Network Share is a very simple and **unsecure** share tools on local network for Linux (it is possible to use on windows, but not with these tools). You can share and receive files or folder in a simple clic.

This tool is based on the following python command which allows to share a folder with the http protocol :
`python3 -m http.server`

The tools is composed by two tool : the server, and the client designed for easy use ;)



#### Installation :
Just run the install.sh script.

Run the following commands :

```
git clone https://gitlab.com/Breizhux/pynesha.git
cd pynesha
chmod +x install.sh
./install.sh
```



#### Uninstall :
Just run following commands :

```
#if you loss the install.sh file, download it :
wget https://gitlab.com/Breizhux/pynesha/-/raw/master/install.sh
chmod +x install.sh
#unsintall
./install.sh -r
```



### PyNeShaS

![Capture de l'interface de partage](img/pyneshas.png "Interface de partage")

The server can just share a folder. In caja file manager, just right-clic on a file, go to script, and clic "NetworkShare"

PyNeShaS.sh is an old simple shell script. Use python to share folder, zenity to display gui, xclip to copy url on clipboard and notify-send for notification.

Now, PyNeShaSGui is a python script that use Gtk. Find the first available port after 8000 to use it for share the directory. You can choose it manually. The link is automatically copy in your pyperclip.


You can access to shared folder with any browser. But the PyNeShaC client allow you to download multiple files, and allow to download directory recusively in many clics...



### PyNeShaC
The client can download files and directory with many options.

You can use it in command line :

```
# Display manual :
python3 PyNeShaC.py -h

# Default usage (download all remote files, not recursively) :
python3 PyNeShaC.py http://192.168.1.10:8000/

# Just list remote files and dirs :
python3 PyNeShaC.py http://192.168.1.10:8000/ -l
#with -d option, you list and download remote files

# Download all remote files (-n to no follow remote tree structure)
python3 PyNeShaC.py http://192.168.1.10:8000/ -R
```


Or you can use the graphical interface !!

![Capture de l'interface de téléchargement](img/pyneshac.png "Interface de téléchargement")