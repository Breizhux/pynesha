#!/bin/bash

INSTALL_DIR="$HOME/.local/share/pynesha"
INSTALL_CLIENT="$INSTALL_DIR/client"
INSTALL_SERVER="$INSTALL_DIR/server"
CLIENT_ACCESS="$HOME/.local/share/applications/PyNeShaC.desktop"
SERVER_ACCESS="$HOME/.config/caja/scripts/NetworkShare"

test_python_module() {
    python3 -c "import $1"
    if [ "$?" -ne 0 ]; then
        echo "You must install $1 for python :"
        echo "sudo apt update && sudo apt install -y $2"
    fi
}

install() {
    #test python dependency
    test_python_module "gi" "python3-gi"
    test_python_module "pyperclip" "python3-pyperclip"
    #create install directory
    mkdir -p "$INSTALL_CLIENT"
    mkdir -p "$INSTALL_SERVER"
    #copy files
    cp -R "./PyNeShaC/"* "$INSTALL_CLIENT"
    cp -R "./PyNeShaS/"* "$INSTALL_SERVER"
    chmod u+x "$INSTALL_CLIENT/pyneshac.sh"
    #create .desktop file for client
    cat > "$CLIENT_ACCESS" << EOF
[Desktop Entry]
Name=PyNeShaC
Name[fr_FR]=PyNeShaC
GenericName=Share folder by network
GenericName[fr]=Partager des fichiers sur le réseau
Comment=Share easily folders on local network with python
Comment[fr]=Partager facilement des fichiers sur le réseau local avec python
Exec=/bin/bash $INSTALL_CLIENT/pyneshac.sh
Type=Application
Icon=network-transmit-receive
Categories=Utility;Network;
Keywords=share;network;pyneshac;
EOF
    #create access from right clic on caja
    cat > "$SERVER_ACCESS" << EOF
#! /bin/bash
cd "$INSTALL_SERVER/"
python3 "PyNeShaSGui.py" "\$CAJA_SCRIPT_SELECTED_FILE_PATHS"
EOF
    chmod u+x "$SERVER_ACCESS"
}

uninstall() {
    rm -r "$INSTALL_CLIENT"
    rm -r "$INSTALL_SERVER"
    rm -R "$INSTALL_DIR"
    rm "$SERVER_ACCESS"
    rm "$CLIENT_ACCESS"
    rm -R "$HOME/.cache/PyNeShaC"
}

if [ $(echo "$*" | grep "\-r") ]; then
    uninstall && echo "Désinstallation terminé." || echo "Erreur..."
else
    install && echo "Installation terminé." || echo "Erreur..."
fi
