#!/usr/bin/python3
# -*- coding : utf-8 -*-

import sys
from time import sleep
from PyNeShaC import *
from subprocess import Popen, PIPE

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Threaded(Thread) :
    """ Class to run function in thread with the possibility
    to get output easily """
    def __init__(self, func, *args, **kwargs) :
        Thread.__init__(self)
        self.function = func
        self.args = args
        self.kwargs = kwargs
        self.result = None
    def run(self) :
        self.result = self.function(*self.args, **self.kwargs)

class RemoteItem(Gtk.CheckButton) :
    """ CheckButton for the list of available remote
    files and folders"""
    def __init__(self, fileinfos) :
        Gtk.CheckButton.__init__(self)
        self.url = fileinfos[0]
        self.path = fileinfos[1]
        self.name = fileinfos[2]
        self.set_label(self.name)
    def isdir(self) :
        return True if self.name[-1] == "/" else False
    def get_infos(self) :
        return (self.url, self.path, self.name)

class PyNeShaCGui :
    """ Gui for PyNeShaC """
    OPTIONS_PATH = "{}/.cache/PyNeShaC/latest.config".format(os.environ['HOME'])
    WINDOW_OBJS = {
        'main_file' : "PyNeShaCGui.glade",
        'window' : "PyNeShaCWindow",
        'ipmenu' : "BaseIp",
        'ipspin' : "EndIp",
        'ipvalue' : "IpAdjustment",
        'portspin' : "NrPort",
        'portvalue' : "PortAdjustment",
        'modemenu' : "download_mode",
        'file_chooser' : "file_chooser",
        'follow_tree' : "CheckTree",
        'availablelist' : "ListBox",
        'progresslabel' : "ProgressLabel",
        'progressbar' : "ProgressBar",
        'closebutton' : "close_button",
        'actionbutton' : "download_button",
    }
    DOWNLOAD_DIR = [
        "Downloads",
        "Téléchargements",
    ]
    LABEL_MESSAGE = {
        'default' : "Lancez pour télécharger la sélection !",
        'server_not_found' : "Erreur : Le serveur n'est pas joignable",
        'url_not_valid' : "Erreur 404 : L'url n'est pas valide.",
        'no_files' : "Aucun fichiers distant disponible.",
        'refresh_list' : "Chargement des fichiers distants...",
        'start_download' : "Lancement du téléchargement...",
        'downloading' : "[{i}/{total}] {name} : {eta}s",
        'end_download' : "Téléchargement terminé !"
    }
    def __init__(self) :
        self.life = True
        interface = Gtk.Builder()
        interface.add_from_file(self.WINDOW_OBJS['main_file'])
        self.window = interface.get_object(self.WINDOW_OBJS['window'])
        self.window.connect("delete-event", self.main_quit)
        self.ipmenu = interface.get_object(self.WINDOW_OBJS['ipmenu'])
        self.ipspin = interface.get_object(self.WINDOW_OBJS['ipspin'])
        self.ipvalue = interface.get_object(self.WINDOW_OBJS['ipvalue'])
        self.portspin = interface.get_object(self.WINDOW_OBJS['portspin'])
        self.portvalue = interface.get_object(self.WINDOW_OBJS['portvalue'])
        self.modemenu = interface.get_object(self.WINDOW_OBJS['modemenu'])
        self.destination = interface.get_object(self.WINDOW_OBJS['file_chooser'])
        self.follow_tree = interface.get_object(self.WINDOW_OBJS['follow_tree'])
        self.availablelist = interface.get_object(self.WINDOW_OBJS['availablelist'])
        self.progresslabel = interface.get_object(self.WINDOW_OBJS['progresslabel'])
        self.progressbar = interface.get_object(self.WINDOW_OBJS['progressbar'])
        self.set_options()
        self.window.show_all()
        interface.connect_signals(self)
        self.refresh_list()

    def main_quit(self, *args) :
        self.save_options()
        self.window.destroy()
        self.life = False

    def _by_thread(self, func, *args, **kwargs) :
        """ Return a result of function by a thread to
        not block interface """
        geter = Threaded(func, *args, **kwargs)
        geter.start()
        while geter.is_alive() :
            Gtk.main_iteration_do(False)
            sleep(0.01)
        if isinstance(geter.result, tuple) :
            return list(geter.result)
        return geter.result

    def run(self, *args) :
        """ Start the download """
        self.progresslabel.set_text(self.LABEL_MESSAGE['start_download'])
        self.refresh_options()
        #Get all files to download from list of available
        files = self.get_files_selection()
        total_files = len(files)
        #Download all files
        for i, file in enumerate(files) :
            #Get future path
            if self.options['follow_tree'] :
                outpath = "{}/{}".format(self.options['directory'], file[1])
            else :
                outpath = "{}/{}".format(self.options['directory'], file[2])
            #run download in thread
            print("[Download] {}".format(outpath))
            downloader = PyNeShaC(file[0], outpath, file[2])
            downloader.start()
            #update gui
            while not downloader.finish :
                Gtk.main_iteration_do(False)
                infos = downloader.get_advancement()
                text = self.LABEL_MESSAGE['downloading'].format(
                    i=i+1, total=total_files, name=file[2], eta=infos[1])
                self.progresslabel.set_text(text)
                self.progressbar.set_fraction(infos[0]*0.01)
                sleep(0.01)
            self.progressbar.set_fraction(1)
            del downloader
        self.progresslabel.set_text(self.LABEL_MESSAGE['end_download'])

    def refresh_list(self, *args) :
        """ Refresh the list of available dirs and files """
        self.progresslabel.set_text(self.LABEL_MESSAGE['refresh_list'])
        self.refresh_options()
        Gtk.main_iteration_do(False)
        #Get remote dirs and files
        if self.options['mode'] == "recursive_all" :
            dirs, files = [], self._by_thread(getall, self.options['url'])
            if files is None : dirs = None
        else :
            dirs, files = self._by_thread(getdir, self.options['url'])
        #Clear list
        for i in self.availablelist.get_children() :
            self.availablelist.remove(i)
        Gtk.main_iteration_do(False)
        #Verify available dirs and files
        if (dirs, files) == (None, None) :
            self.progresslabel.set_text(self.LABEL_MESSAGE['server_not_found'])
            Gtk.main_iteration_do(False)
            return None
        elif (dirs, files) == (False, False) :
            self.progresslabel.set_text(self.LABEL_MESSAGE['url_not_valid'])
            Gtk.main_iteration_do(False)
            return None
        elif len(dirs)+len(files) == 0 :
            self.progresslabel.set_text(self.LABEL_MESSAGE['no_files'])
            Gtk.main_iteration_do(False)
            return None
        #Add directory
        for i in dirs :
            item = RemoteItem(i)
            if self.options['mode'] == "recursive_all" :
                item.set_active(1)
            self.availablelist.add(item)
        Gtk.main_iteration_do(False)
        #Add files
        for i in files :
            item = RemoteItem(i)
            if self.options['mode'] in ["recursive_all", "simply_all"] :
                item.set_active(1)
            self.availablelist.add(item)
        self.progresslabel.set_text(self.LABEL_MESSAGE['default'])
        self.window.show_all()
        Gtk.main_iteration_do(False)

    def refresh_options(self) :
        """ Refresh options dict """
        self.options = {
            'url' : self.get_url(),
            'mode' : self.modemenu.get_active_id(),
            'directory' : self.destination.get_filename(),
            'follow_tree' : self.follow_tree.get_active(),
        }

    def get_url(self, split=False) :
        """ Return the complete url if split is false
        else the base ip, end ip and port."""
        base = self.ipmenu.get_active_text()
        end = int(self.ipspin.get_value())
        port = int(self.portspin.get_value())
        if split :
            return (base, end, port)
        url = "http://{}.{}:{}/".format(base, end, port)
        return url

    def get_files_selection(self) :
        """ Return the tuple of files for download """
        files = []
        for i in self.availablelist :
            child = i.get_child()
            if child.get_active() :
                if child.isdir() :
                    recursive_files = getall(child.url)
                    result_files = []
                    for i in recursive_files :
                        path = "{}{}".format(child.name, i[1])
                        result_files.append((i[0], path, i[2]))
                    files += result_files
                else : files.append(child.get_infos())
        return files

    def save_options(self) :
        if not os.path.exists(os.path.dirname(self.OPTIONS_PATH)) :
            os.makedirs(os.path.dirname(self.OPTIONS_PATH))
        with open(self.OPTIONS_PATH, 'w') as file :
            file.write("{}\n{}\n{}\n".format(*self.get_url(split=True)))
            file.write("{}\n".format(self.modemenu.get_active_id()))
            file.write("{}\n".format(self.destination.get_filename()))
            if self.follow_tree.get_active() : file.write("1\n")
            else : file.write("0\n")

    def set_latest_options(self, ips) :
        try :
            with open(self.OPTIONS_PATH, 'r') as file :
                #Set IP
                base = file.readline()[:-1]
                end = file.readline()[:-1]
                port = file.readline()[:-1]
                if base in ips :
                    self.ipmenu.set_active(ips.index(base))
                    self.ipspin.set_value(int(end))
                    self.ipspin.select_region(0,10)
                    self.portspin.set_value(int(port))
                #Set mode
                self.modemenu.set_active_id(file.readline()[:-1])
                #Set directory
                directory = file.readline()[:-1]
                if os.path.exists(directory) :
                    self.destination.set_filename(directory)
                #Set if follow tree
                self.follow_tree.set_active(int(file.readline()[:-1]))
        except : pass

    def set_options(self) :
        """ Complete ipmenu with the base of ip in using
        by system. And complete destination by download folder.
        Other options set by glade. If config file found, set
        the option from this."""
        #IP MENU
        command = "hostname -I"
        process = Popen(command, stdout=PIPE, shell=True, encoding='utf-8')
        ips = []
        for i in process.stdout.readlines()[0].replace('\n', ' ').strip(' ').split(' ') :
            baseip = i[:i.rfind('.')]
            if not baseip in ips :
                ips.append(baseip)
        for ip in ips :
            self.ipmenu.append_text(ip)
        self.ipmenu.set_active(0)
        #DESTINATION
        home = os.environ['HOME']
        for i in self.DOWNLOAD_DIR :
            path = "{}/{}".format(home, i)
            if os.path.exists(path) : break
            else : path = home
        self.destination.set_filename(path)
        # CONFIG FILE
        if os.path.exists(self.OPTIONS_PATH) :
            self.set_latest_options(ips)

if __name__ == "__main__" :
    Gui = PyNeShaCGui()
    while Gui.life :
        try : Gtk.main_iteration_do(False)
        except : break
        sleep(0.001)
    Gtk.main_quit()
