#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
import re
import sys
import requests
import subprocess
from time import sleep
from threading import Thread

MANUEL = """
usage : python3 client.py <url of python shared directory> <options>

options :
    -h | --help : show this manuel.
    -l | --list : just list available directory and files.
    -d | --download : after '-l' option, force download.
    -r | --recursive : find files recursively.
    -n | --no-tree : when downloading files recursively, do not follow remote tree structure.
    -O | --output <folder path> : download in the folder path.

Without any options, the client download all files available without recursive, just
  current remote directory.
"""

REG_FILE_LIST = re.compile("<li><a href=\"(?P<LINK>.*)\">(?P<NAME>.*)<\/a><\/li>")

def getfile(url) :
    """ Return a tuple (link, human path, human name) """
    urlparse = requests.utils.urlparse(url)
    path = urlparse.path
    name = os.path.basename(path)
    return (url, path, name)

def getdir(url, cropat=0) :
    """ Return 2 lists : dirs and files
    There are list of tuple : (link, human path, human name) """
    if cropat == 0 : cropat = len(url)
    try :
        index = requests.get(url)
    except requests.exceptions.ConnectionError :
        print("[ERREUR] Le serveur python http est-il bien démarré ?")
        return None, None
    if index.status_code == 404 :
        print("[ERREUR] L'url n'est pas valide. Vérifiez qu'il s'agit d'un dossier.")
        return False, False
    available = REG_FILE_LIST.findall(index.text)
    dirs = []
    files = []
    for i in available :
        link = "{}{}".format(url, i[0])
        path = "{}{}".format(url[cropat:], i[1])
        if i[0][-1] == "/" :
            dirs.append((link, path, i[1]))
        else :
            files.append((link,  path, i[1]))
    return dirs, files

def getall(url, cropat=0) :
    """ Return all avalaible files recursively"""
    if cropat == 0 : cropat = len(url)
    result = []
    dirs, files = getdir(url, cropat=cropat)
    if files is None : return None
    result += files
    for i in dirs :
        files = getall(i[0], cropat=cropat)
        result += files
    return result

def write_remote_file(url, output) :
    """ Write remote file in local file """
    if os.path.exists(output) :
        i = 1
        new_path = "{}({})"
        while os.path.exists(new_path.format(output, i)) :
            i += 1
        output = new_path
    r = requests.get(url, stream=True)
    total_length = int(r.headers.get('content-length'))
    total_downloaded = 0
    with open(output, 'wb') as f :
        for line in r.iter_lines() :
            total_downloaded += len(line)
            percent = round((total_downloaded/total_length)*100, 4)
            print("[{}%] {}/{}".format(percent, total_downloaded, total_length))
            f.write(line)
            f.flush()

def download(file, dir=os.getcwd(), all_in_dir=False) :
    """ Download a file
    file (tuple) : file informations (link, human path, human name)
    dir : directory to download in
    follow_tree (bool) : keep the remote tree structure"""
    if dir[-1] != "/" : dir += "/"
    if all_in_dir :
        outname = "{}{}".format(dir, file[2])
    else :
        outname = "{}{}".format(dir, file[1])
    if not os.path.exists(os.path.dirname(outname)) :
        os.makedirs(os.path.dirname(outname))
    write_remote_file(file[0], outname)

def download_all(url, dir=os.getcwd(), all_in_dir=False) :
    """ Download all files recursively
    url : source
    dir : destination directory
    follow_tree (bool) : keep the remote tree structure"""
    if url[-1] != "/" : url += "/"
    files = getall(url)
    for i in files :
        download(i, dir=dir, all_in_dir=all_in_dir)



class PyNeShaC(Thread) :
    """ PyNeShaC in Thread for download advancement """
    RE_INFO = re.compile("(.*) (?P<size>[0-9]+) (.*) \[(?P<type>[a-z\/]+)\]")
    RE_PROGRESS = re.compile("(?P<bits>[0-9A-Z]+)(.*) (?P<percent>[0-9]+)%(.*) (?P<eta>[0-9]+)s")
    COMMAND_BASE = "wget --progress=dot {url} -O \"{output}\""
    def __init__(self, url, output, name) :
        Thread.__init__(self)
        self.url = url
        self.output = self.check_output(output)
        self.name = name
        self.finish = False
        self.command = self.get_command()
        #value changed by command output
        self.total_length = 0
        self.read_length = 0
        self.percent = 0
        self.filetype = ""
        self.eta = ""

    def run(self) :
        print(self.command)
        self.process = subprocess.Popen(self.command, stdout=None, stderr=subprocess.PIPE, shell=True, encoding='utf8')
        while self.process.poll() is None :
            line = self.process.stderr.readline().strip(" ")
            if self.total_length == 0 :
                match = self.RE_INFO.match(line)
                if match is not None :
                    r = match.groupdict()
                    self.size = int(r['size'])
                    self.filetype = r['type']
            match = self.RE_PROGRESS.match(line)
            if match is not None :
                r = match.groupdict()
                self.percent = int(r['percent'])
                self.eta = int(r['eta'])
                self.read_length = r['bits']
        self.read_length = self.total_length
        self.percent = 100
        self.eta = 0
        self.finish = True

    def get_command(self) :
        return self.COMMAND_BASE.format(url=self.url,
                                        output=self.output)

    def check_output(self, path) :
        dirpath = os.path.dirname(path)
        if not os.path.exists(dirpath) and dirpath != "" :
            os.makedirs(os.path.dirname(path))
        if os.path.exists(path) :
            new_path = "{path}.{n}"
            i = 1
            test_path = new_path.format(path=path, n=i)
            while os.path.exists(test_path) :
                i += 1
                test_path = new_path.format(path=path, n=i)
            path = test_path
        return path

    def get_advancement(self) :
        return [self.percent, self.eta]



def parse_argv(args) :
    OPTIONS = {
        'url' : "",
        'list' : False,
        'download' : True,
        'recursive' : False,
        'all_in_dir' : False,
        'directory' : os.getcwd(),
    }
    key = ""
    for i in args :
        # clés
        if i[0] == '-' :
            key = i
            if key in ['-h', '--help'] :
                print(MANUEL)
                sys.exit(0)
            elif key in ['-l', '--list'] :
                OPTIONS['list'] = True
                OPTIONS['download'] = False
            elif key in ['-d', '--download'] :
                OPTIONS['download'] = True
            elif key in ['-R', '-r', '--recursive'] :
                OPTIONS['recursive'] = True
            elif key in ['-n', '--no-tree'] :
                OPTIONS['all_in_dir'] = True
        # valeurs
        else :
            if key == "" :
                OPTIONS['url'] = i
            elif key in ['-O', '--output'] :
                OPTIONS['directory'] = i
    return OPTIONS

if __name__ == "__main__" :
    args = parse_argv(sys.argv)

    if args['recursive'] :
        dirs, files = [], getall(args['url'])
    elif args['url'][-1] == "/" :
        dirs, files = getdir(args['url'])
    else :
        dirs, files = [], getfile(args['url'])

    if args['list'] :
        lgth_base = len(args['url'])
        for i in dirs :
            print(i[1][lgth_base:])
        print("\n")
        for i in files :
            print(i[1])

    if args['download'] :
        for i in files :
            download(i, dir=args['directory'], all_in_dir=args['all_in_dir'])
