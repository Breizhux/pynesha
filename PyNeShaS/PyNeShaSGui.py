#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
import sys
import socket
import pyperclip
from time import sleep
from subprocess import Popen, PIPE

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class PyNeShaSGui :
    """ Gui for PyNeShaC """
    WINDOW_OBJS = {
        'main_file' : "PyNeShaSGui.glade",
        'window' : "PyNeShaSWindow",
        'ip' : "IpAddress",
        'ipvalue' : "IpAdjustement",
        'portspin' : "NrPort",
        'portvalue' : "PortAdjustment",
        'label' : "MainLabel",
        'urllabel' : "UrlLabel",
        'file_chooser' : "SharedSource",
        'closebutton' : "stopshare",
        'actionbutton' : "reset",
    }
    LABEL_MESSAGE = {
        'sharing' : "Partage en cours !",
        'no_network' : "Erreur : pas de réseau disponible !",
        'stopping' : "Arrêt du partage en cours...",
        'stopped' : "Partage terminé !",
        'reset' : "Redémarrage du partage...",
        'prepare' : "Préparation du partage..."
    }
    TIMER = 0.02
    def __init__(self, shared) :
        self.life = True
        self.command = "./PyNeShaS.sh \"{}\" {}"
        self.process = None
        #attributes
        self.shared_path = shared
        self.actual_ip = None
        self.actual_port = None
        self.actual_pid = None
        #gui
        interface = Gtk.Builder()
        interface.add_from_file(self.WINDOW_OBJS['main_file'])
        self.window = interface.get_object(self.WINDOW_OBJS['window'])
        self.window.connect("delete-event", self.main_quit)
        self.ipmenu = interface.get_object(self.WINDOW_OBJS['ip'])
        self.ipvalue = interface.get_object(self.WINDOW_OBJS['ipvalue'])
        self.portspin = interface.get_object(self.WINDOW_OBJS['portspin'])
        self.portvalue = interface.get_object(self.WINDOW_OBJS['portvalue'])
        self.mainlabel = interface.get_object(self.WINDOW_OBJS['label'])
        self.iplabel = interface.get_object(self.WINDOW_OBJS['urllabel'])
        self.sharedsource = interface.get_object(self.WINDOW_OBJS['file_chooser'])
        self.sharedsource.set_filename(self.shared_path)
        self._init_options()
        self.window.show_all()
        interface.connect_signals(self)
        #start sharing
        #self.startshare()

    def main_quit(self, *args) :
        self.stopshare()
        self.window.destroy()
        self.life = False

    def startshare(self) :
        self.mainlabel.set_text(self.LABEL_MESSAGE['prepare'])
        #Get options
        self._set_ip()
        self._set_port()
        self._set_source()
        # start process
        self.process = Popen(
            self.command.format(self.shared_path, self.actual_port),
            stdout=None, stderr=PIPE, shell=True, encoding='utf8')
        self._set_pid()
        self.iplabel.set_text(
            "http://{}:{}/".format(self.actual_ip, self.actual_port))
        self._set_pyperclip()
        self.mainlabel.set_text(self.LABEL_MESSAGE['sharing'])

    def stopshare(self, *args) :
        """ Stop sharing """
        self.mainlabel.set_text(self.LABEL_MESSAGE['stopping'])
        with open("/tmp/pyneshas.tmp", 'w') as file :
            file.write(self.actual_pid)
        while os.path.exists("/tmp/pyneshas.tmp") :
            sleep(self.TIMER)
        del self.process
        self.process = None
        self.mainlabel.set_text(self.LABEL_MESSAGE['stopped'])

    def restart(self, *args) :
        """ Start the sharing server """
        self.stopshare()
        self.startshare()

    def _init_options(self) :
        #IP MENU
        command = "hostname -I"
        process = Popen(command, stdout=PIPE, shell=True, encoding='utf-8')
        for i in process.stdout.readlines()[0].replace('\n', '').strip(' ').split(' ') :
            self.ipmenu.append_text(i)
        self.ipmenu.set_active(0)

    def _set_ip(self) :
        self.actual_ip = self.ipmenu.get_active_text()

    def _set_port(self) :
        self.actual_port = int(self.portspin.get_value())
        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        check_result = a_socket.connect_ex((self.actual_ip, self.actual_port))
        while check_result == 0 :
            self.actual_port += 1
            check_result = a_socket.connect_ex((self.actual_ip, self.actual_port))
        self.portspin.set_value(self.actual_port)

    def _set_source(self) :
        self.shared_path = self.sharedsource.get_filename()
        #TODO : if is file, create link and share just the file

    def _set_pid(self) :
        while not os.path.exists("/tmp/pyneshaspid.tmp") : sleep(self.TIMER)
        with open("/tmp/pyneshaspid.tmp") as file :
            self.actual_pid = file.read().replace('\n', '').strip(' ')
        os.remove("/tmp/pyneshaspid.tmp")

    def _set_pyperclip(self) :
        pyperclip.copy(self.iplabel.get_text())


if __name__ == "__main__" :
    #determination of shared source
    if "CAJA_SCRIPT_SELECTED_FILE_PATHS" in os.environ :
        shared = os.environ["CAJA_SCRIPT_SELECTED_FILE_PATHS"].strip('\n')
    elif len(sys.argv) > 1 :
        shared = sys.argv[-1]
    else :
        print("usage : python3 PyNeShaSGui.py \"shared/path\"")
        sys.exit(1)
    if os.path.isfile(shared) : shared = os.path.dirname(shared)
    #create gui
    Gui = PyNeShaSGui(shared=shared)
    Gui.startshare()
    #while users not close
    while Gui.life :
        try : Gtk.main_iteration_do(False)
        except : break
        sleep(0.01)
    Gtk.main_quit()
