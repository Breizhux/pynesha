#!/bin/bash

share_directory() {
    python3 -m http.server $1 &
    echo $! > "/tmp/networkshare.tmp"
}

#Go to the directory
directory="$1"
cd "$directory"

#Get a available port, ip and link
port=$2

#Share the directory with python and get the pid of shared process
share_directory $port
pid_share=$(cat "/tmp/networkshare.tmp")
mv "/tmp/networkshare.tmp" "/tmp/pyneshaspid.tmp"

while true; do
    if [ -e "/tmp/pyneshas.tmp" ]; then
        if [ "$(cat /tmp/pyneshas.tmp)" == "$pid_share" ]; then
            break
        fi
    fi
    sleep 0.5
done

#Stop folder sharing
kill -9 "$pid_share"
rm "/tmp/pyneshas.tmp"

#Verify if sharing have good terminate
nc -vvv -w 10 -z $(hostname -I | cut -d " " -f 1) $port > /dev/null 2> /dev/null
if [ $? -ne 1 ]; then
    texte="<b><big>ATTENTION : Le port $port sur $ip est toujours ouvert ! </big></b>
pid du processus : $pid_share"
    zenity --error --text "$texte" --width 220
fi
